.. piperci documentation master file, created by
   sphinx-quickstart on Tue Jul 30 16:35:02 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the PiperCI documentation!
=====================================

Overview
--------

PiperCI is a CI/CD management framework that manages the pipelines, stages, tasks, resources, and infrastructure typically associated with software CI/CD. The framework includes both client-side components (software installed on an individual user’s workstation) and server-side components (infrastructure and resources shared by the team) that handles everything from project bootstrapping to software deployment.

Project Goals
-------------

The PiperCI platform aims to
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- allow the running of pipelines before pushing code into a source code manager (SCM)
- provide CI/CD infrastructure as if it were on the local workstation using a simple CLI interface
- work with a traditional/existing CI/CD orchestrator pipelines only replacing the logic that PiperCI handles better
- bolt-on to existing workflows easily with minimal effort
- aid in the transfer of pipelines to different CI/CD orchestrators while still getting the same result
- allow the running of individual pipeline stages against local code (think `tox -e lint` remotely)
- eliminate the "it works on my machine" problem

Project goals and guiding principles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- For each supported pipeline stage and resource, have good sensible defaults based on familiar common standards[]
- Strive for Simplicity and Speed
- The infrastructure that runs the system should be installable and upgradable as easily and as quickly as possible
- The system should require as little client-side configuration as possible
- The system should validate inputs to resources as its first step, to fail early if there input would cause a fatal issue in a later step
- The system should be version controlled so to avoid surprises when rehydrating in another network or at a future date

Implementation
--------------

PiperCI's primary component is the PiCli CLI which parses and controls the execution flow for CI/CD pipelines provided stages are implemented in `OpenFaaS <https://www.openfaas.com/>`__ although any web reachable endpoints may be used with PiCli so long as they implement the PiperCI API function interface. Additionally process state is maintained by :doc:`GMan </gman/README>`, and artifacts are stored in `MinIO <https://min.io/download>`__

Deployment
----------

:doc:`Piperci Installer </piperci-installer/README>`

.. toctree-filter::
   :maxdepth: 2
   :caption: Contents:
   :glob:
   :exclude-patterns: .*-faas/README faas-templates/README

   piperci-functions
   */README
   project-info/LICENSE
   project-info/contributing


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
